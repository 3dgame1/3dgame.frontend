import { KeyDisplay } from "./utils";
import { CharacterControls } from "./characterControls";
import * as THREE from "three";
import { CameraHelper, ObjectSpaceNormalMap } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";





function guid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
  });
}

// SCENEw
const scene = new THREE.Scene();
scene.background = new THREE.Color(0xa8def0);

var balls: any[] = new Array();

// CAMERA
const camera = new THREE.PerspectiveCamera(
  45,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
camera.position.y = 5;
camera.position.z = 5;
camera.position.x = 0;

// RENDERER
const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.shadowMap.enabled = true;

// CONTROLS
const orbitControls = new OrbitControls(camera, renderer.domElement);
orbitControls.enableDamping = true;
orbitControls.minDistance = 5;
orbitControls.maxDistance = 10;
orbitControls.enablePan = false;
orbitControls.maxPolarAngle = Math.PI / 2 - 0.05;
orbitControls.update();

// LIGHTS
light();



var treePositions = new Array();
var smallTreePositions = new Array();
if(localStorage.getItem('tree') == null)
{
 
  for(let i = 0; i < 15; i++){
    treePositions.push({x:getRandomArbitrary(-200, 200) , y:getRandomArbitrary(-200, 200) });
    smallTreePositions.push({x:getRandomArbitrary(-200, 200) , y:getRandomArbitrary(-200, 200) })
  }
  localStorage.setItem('tree', JSON.stringify({small: smallTreePositions, big: treePositions}));
}
else{
  let data = JSON.parse(localStorage.getItem('tree'));
  treePositions = data.big;
  smallTreePositions = data.small;
}



// MODEL WITH ANIMATIONS
var characterControls: CharacterControls;
new GLTFLoader().load("models/Soldier.glb", function (gltf) {
  const model = gltf.scene;
  model.traverse(function (object: any) {
    if (object.isMesh) object.castShadow = true;
  });
  scene.add(model);

  const gltfAnimations: THREE.AnimationClip[] = gltf.animations;
  const mixer = new THREE.AnimationMixer(model);
  const animationsMap: Map<string, THREE.AnimationAction> = new Map();
  gltfAnimations
    .filter((a) => a.name != "TPose")
    .forEach((a: THREE.AnimationClip) => {
      animationsMap.set(a.name, mixer.clipAction(a));
    });
  characterControls = new CharacterControls(
    model,
    mixer,
    animationsMap,
    orbitControls,
    camera,
    "Idle"
  );
  getFromDb();
  createSky();
});

// CONTROL KEYS
const keysPressed = {};
const keyDisplayQueue = new KeyDisplay();

function remove(id: string) {
  scene.remove(scene.getObjectByName(id));
}

function getFromDb() {
  if(localStorage.getItem('userKey') != null){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let data = JSON.parse(this.responseText);
        characterControls.model.position.set(data.playerPos.x, data.playerPos.y, data.playerPos.z);
        balls = data.objectPos.map((p: any) => p.position = p);
        document.getElementById("points").innerText = `${balls.length} balls`;
        generateFloor();
      }
    };
    xhttp.open("GET", `http://localhost:3000/${localStorage.getItem('userKey')}`, true);
    xhttp.send();
  }
  else{
    generateFloor();
  }
  
}

function saveInDb(data: any) {
  if(localStorage.getItem('userKey') == null){
    localStorage.setItem('userKey', guid());

    data.userId = localStorage.getItem('userKey');

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
    
    };
    xhttp.open("POST", "http://localhost:3000");
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(JSON.stringify(data) as any);
  }
  else{
    data.userId = localStorage.getItem('userKey');
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
    
    };
    xhttp.open("PUT", `http://localhost:3000/${localStorage.getItem('userKey')}`);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(JSON.stringify(data) as any);
  }
  
}

function createSky() {
 
  let materialArray = [];
  let texture_ft = new THREE.TextureLoader().load( './textures/sky/arid_ft.jpg');
  let texture_bk = new THREE.TextureLoader().load( './textures/sky/arid_bk.jpg');
  let texture_up = new THREE.TextureLoader().load( './textures/sky/arid_up.jpg');
  let texture_dn = new THREE.TextureLoader().load( './textures/sky/arid_dn.jpg');
  let texture_rt = new THREE.TextureLoader().load( './textures/sky/arid_rt.jpg');
  let texture_lf = new THREE.TextureLoader().load( './textures/sky/arid_lf.jpg');
    
  materialArray.push(new THREE.MeshBasicMaterial( { map: texture_ft }));
  materialArray.push(new THREE.MeshBasicMaterial( { map: texture_bk }));
  materialArray.push(new THREE.MeshBasicMaterial( { map: texture_up }));
  materialArray.push(new THREE.MeshBasicMaterial( { map: texture_dn }));
  materialArray.push(new THREE.MeshBasicMaterial( { map: texture_rt }));
  materialArray.push(new THREE.MeshBasicMaterial( { map: texture_lf }));

  
  

  for (let i = 0; i < 6; i++)
     materialArray[i].side = THREE.BackSide;
  let skyboxGeo = new THREE.BoxGeometry( 1000, 1000, 1000);
  let skybox = new THREE.Mesh( skyboxGeo, materialArray );
  scene.add( skybox );  
  animate();
}

document.addEventListener(
  "keydown",
  (event) => {
    keyDisplayQueue.down(event.key);
    if (event.shiftKey && characterControls) {
      characterControls.switchRunToggle();
    } else {
      (keysPressed as any)[event.key.toLowerCase()] = true;
    }
  },
  false
);
document.addEventListener(
  "keyup",
  (event) => {
    keyDisplayQueue.up(event.key);
    (keysPressed as any)[event.key.toLowerCase()] = false;
  },
  false
);

const clock = new THREE.Clock();
// ANIMATE
function animate() {
  let mixerUpdateDelta = clock.getDelta();
  if (characterControls) {
    characterControls.update(mixerUpdateDelta, keysPressed);

    for (let i = 0; i < balls.length; i++) {
      if (characterControls.model.position.distanceTo(balls[i].position) < 2) {
        remove(balls[i].name);
        balls = balls.filter((b) => b.name != balls[i].name);
        document.getElementById("points").innerText = `${balls.length} balls`;


        let data = {
          "userId": null as any,
          "playerPos": {
              "x": characterControls.model.position.x,
              "y": characterControls.model.position.y,
              "z": characterControls.model.position.z
          },
          "objectPos": new Array()
        };
      
        for(let i = 0; i < balls.length;i++){
          data.objectPos.push({
            x: balls[i].position.x,
            y: balls[i].position.y,
            z: balls[i].position.z,
          })
        }
        
        saveInDb(data);
      }
    }
  }
  
  orbitControls.update();
  renderer.render(scene, camera);
  requestAnimationFrame(animate);
}
document.body.appendChild(renderer.domElement);
animate();

// RESIZE HANDLER
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
  keyDisplayQueue.updatePosition();
}
window.addEventListener("resize", onWindowResize);

function generateFloor() {
  const textureLoader = new THREE.TextureLoader();
  var floorTexture = textureLoader.load("./textures/sand/Sand 002_COLOR.jpg");
  floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping;
  floorTexture.repeat.set(100, 100);
  var floorMaterial = new THREE.MeshStandardMaterial({
    map: floorTexture,
    side: THREE.DoubleSide,
  });
  var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
  var mesh = new THREE.Mesh(floorGeometry, floorMaterial);
  mesh.rotation.x = -Math.PI / 2;
  mesh.receiveShadow = true;
  scene.add(mesh);

  var ballMat = new THREE.MeshStandardMaterial({
    color: 0xffffff,
    roughness: 0.5,
    metalness: 1.0,
  });
  if(balls != null && balls.length > 0){
    for (let i = 0; i < balls.length; i++) {
      const ballGeometry = new THREE.SphereGeometry(0.25, 32, 32);
      const ballMesh = new THREE.Mesh(ballGeometry, ballMat);
      let position = balls[i].x;
      ballMesh.position.set(position, 0.25, balls[i].z);
      ballMesh.rotation.y = Math.PI;
      //ballMesh.castShadow = true;
      ballMesh.name = position.toString();
      scene.add(ballMesh);
      balls[i].name = ballMesh.name;
    }
  }
  else{
    for (let i = 0; i < 300; i++) {
      const ballGeometry = new THREE.SphereGeometry(0.25, 32, 32);
      const ballMesh = new THREE.Mesh(ballGeometry, ballMat);
      let position = getRandomArbitrary(-100, 100);
      ballMesh.position.set(position, 0.25, getRandomArbitrary(-100, 100));
      ballMesh.rotation.y = Math.PI;
      //ballMesh.castShadow = true;
      ballMesh.name = position.toString();
      scene.add(ballMesh);
      balls.push(ballMesh);
    }
  }

 
    new GLTFLoader().load("models/tree/scene.gltf", function (gltf) {
      for (let i = 0; i < 15; i++) {
        const model = gltf.scene ;
        model.position.set(treePositions[i].x, 2.5, treePositions[i].y);
        model.traverse(function (object: any) {
          if (object.isMesh) object.castShadow = true;
        });
        model.scale.set( 5, 5, 5 );
        scene.add(model.clone());
      }
  });

  new GLTFLoader().load("models/big-tree/scene.gltf", function (gltf) {
    for (let i = 0; i < 15; i++) {
      const model = gltf.scene ;
      model.position.set(smallTreePositions[i].x, 4, smallTreePositions[i].y);
      model.traverse(function (object: any) {
        if (object.isMesh) object.castShadow = true;
      });
      model.scale.set( 5, 5, 5 );
      scene.add(model.clone());
    }
});



  //obj.scale.set( .001, .001, .001 );



  document.dispatchEvent(new KeyboardEvent('keydown',{'key':'w'}));
  setTimeout(() => {
    document.dispatchEvent(new KeyboardEvent('keyup',{'key':'w'}));
  }, 0);
  
  
}

function getRandomArbitrary(min: number, max: number) {
  return Math.random() * (max - min) + min;
}

function light() {
  scene.add(new THREE.AmbientLight(0xffffff, 0.7));

  const dirLight = new THREE.DirectionalLight(0xffffff, 1);
  dirLight.position.set(80, 80, 140);
  dirLight.castShadow = true;
  dirLight.shadow.camera.top = 50;
  dirLight.shadow.camera.bottom = -50;
  dirLight.shadow.camera.left = -350;
  dirLight.shadow.camera.right = 350;
  dirLight.shadow.camera.near = 0.1;
  dirLight.shadow.camera.far = 400;
  dirLight.shadow.mapSize.width = 4096;
  dirLight.shadow.mapSize.height = 4096;
  scene.add(dirLight);
  // scene.add( new THREE.CameraHelper(dirLight.shadow.camera))
}
